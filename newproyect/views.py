
from django.http import HttpResponse
import json
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def Regresion(request):

    x = np.array([23.6,22.5,20.4,17,15,14.5,13,9])
    y_act = np.array([83734,89272,96943,103219,116846,125878,208243,239684])

    y_pred = 308256.7296 - 10386.924 * x

    x_list = x.tolist()
    y_pred_list = y_pred.tolist()
    y_act_list = y_act.tolist()

    data = pd.DataFrame(
    {
        "x":x_list,
        "y_actual":y_act_list,
        "y_prediccion":y_pred_list
    }
    )


    return HttpResponse(str(data.head()))

def estimate_b0_b1(x, y):
    n = np.size(x)
    #obtenemos los promedios de X y Y
    m_x, m_y = np.mean(x), np.mean(y)

    #Calcular la sumatoria de XY y mi sumatoria de XX
    sumatoria_xy = np.sum((x-m_x)*(y-m_y))
    sumatoria_xx = np.sum(x*(x-m_x))

    #coeficiente de regresion
    b_1 = sumatoria_xy / sumatoria_xx
    b_0 = m_y - b_1 * m_x

    return (b_0, b_1)


def plot_regresion(x, y, b):
    plt.scatter (x, y, color = "r", marker = "o", s = 30)

    y_pred = b[0] + b[1]*x
    plt.plot(x, y_pred, color = "b")


    for c in x :
        y_pred = b[0] + b[1]*x
        print(y_pred)



    #etiquetado
    plt.ylabel('Produndidad')
    plt.xlabel('Kilometraje')

    plt.show()


def main(request):

    y = np.array([23.6,22.5,20.4,17,15,14.5,13,9])
    x = np.array([83734,89272,96943,103219,116846,125878,208243,239684])
    b = estimate_b0_b1(x,y)
    print("los valores b0 = {}, b1 = {}".format(b[0], b[1]))
    plot_regresion(x, y, b)

    return HttpResponse("valor")

def Modelo(request):

    numbers = request.GET['numbers'].split(',')
    modelo_user = int(numbers[0])
    profundidad = int(numbers[1])
    tolerancia =  int(5)

    print(numbers[0])
    print(numbers[1])
    
    values = {
    'registro_neumaticos': 11,
    'vehiculo': 1,
    'sucursal': 1,
    'empresa':	8,
    'division': 0,
    'almacen': "",
    'condicion': 2,
    'ubicacion': 2,
    'posicion': 6,
    'no_eje': 2,
    'durabilidad_esperada': 250000,
    'proveedor': 5,
    'codigo': 2439,
    'fecha_revision_neumaticos': "13/12/2017 05:54",
    'rfid': "",
    'no_revitalizaciones': 0,
    'refaccion':	0,
    'no_parches':	0,
    'caracteristicas_modelos_tipo_neumatico': 1,
    'modelo': 27,
    'marca_original': 2,
    'profundidad_huella_modelo_org': 14,
    'modelo_revitalizado': "",
    'tipo_neumatico': "",
    'marca_revitalizada': "",
    'profundidad_huella_modelo_rev': "",
    'medida':	1,
    'precio': 5801,
    'precio_renovado': 5801,
    'danos': "",
    'causa_retiro': "",
    'tipo_desgaste': "",
    'kilometraje_del_vehiculo_en_revision': 6160000,
    'con_rendimiento': 1,
    'kilometraje_neumatico': 234876.9,
    'kilometros_ultimo_recorrido': 8239,
    'revisiones_profundidad_profundidad_huella': 10,
    'neumatico_id': 63249,
    'configuracion_no_renovaciones': 0,
    'configuraciones_tolerancia_profundidad': 5,
    'configuraciones_tolerancia_profundidad_direccionales': 5,
    'configuraciones_politica_rotacion': 100000,
    'configuraciones_politica_rotacion_dir': "",
    'configuraciones_tolerancia_presion': 5,
    'configuraciones_tolerancia_presion_inflado': 5,
    'presion_max': 120,
    'presion_recomendada': 110
    }


    #Dataset
    mainpath = "/Users/jonya/Developer/python-ml-course-master/datasets"
    filename = "fasdata/modelos.csv"
    fullpath = os.path.join(mainpath, filename)

    data = pd.read_csv(fullpath)

    subset = data[["registro_neumaticos","codigo", "vehiculo", "ubicacion", "posicion", "no_eje", "fecha_revision_neumaticos", "no_revitalizaciones",
            "refaccion",  "modelo",  "profundidad_huella_modelo_org",
            "tipo_neumatico", "medida", "tipo_desgaste", "kilometraje_del_vehiculo_en_revision", "kilometraje_neumatico",
            "kilometros_ultimo_recorrido", "revisiones_profundidad_profundidad_huella", "configuraciones_tolerancia_profundidad", "neumatico_id"]]

    modelo = subset[subset["modelo"] == modelo_user] #modelo
    modelo["div"] = modelo["kilometraje_neumatico"] / modelo["revisiones_profundidad_profundidad_huella"]
    #promedio de la columna div que ayuda a clasificar los registros de los modelos para que el error se redusca
    #
    prom = np.mean(modelo['div'])
    print(prom)
    minn = prom - 4000
    maxx = prom + 4000
    
    modelo = modelo[modelo["div"]<maxx]
    modelo = modelo[modelo["div"]>minn]
    modelo2 = modelo
    modelo2.plot(kind="scatter", x="revisiones_profundidad_profundidad_huella",  y="kilometraje_neumatico")
    #plt.show()
    x_mean = np.mean(modelo["revisiones_profundidad_profundidad_huella"])
    y_mean = np.mean(modelo["kilometraje_neumatico"])
    x_mean, y_mean
    modelo["modelo_n"] = (modelo["revisiones_profundidad_profundidad_huella"]-x_mean)*(modelo["kilometraje_neumatico"]-y_mean)
    modelo["modelo_b"] = (modelo["revisiones_profundidad_profundidad_huella"]-x_mean)**2
    beta = sum(modelo["modelo_n"])/sum(modelo["modelo_b"])
    alpha = y_mean - beta * x_mean
    alpha, beta
    modelo["res"] = alpha + beta * modelo["revisiones_profundidad_profundidad_huella"]

                                #profundiad                                                 #viene del json
    modelo["desgate_restante"] = modelo['revisiones_profundidad_profundidad_huella'] - modelo['configuraciones_tolerancia_profundidad']
                                        #json que se recibe de la aplicacion movil 
    desgaste_restante = profundidad - values['configuraciones_tolerancia_profundidad']

    modelo["prediccion"] = alpha + beta * modelo["desgate_restante"]

    prediccion = alpha + beta * desgaste_restante

    print(prediccion)
    resultados = modelo[["kilometraje_neumatico","res","profundidad_huella_modelo_org","configuraciones_tolerancia_profundidad", "desgate_restante","prediccion"]]

    kilometraje = modelo[["kilometraje_neumatico"]]
    kilometraje = kilometraje.to_json( indent=4)

    valores_tot = {
        "Prediccion": prediccion,
        "Desgaste_res": desgaste_restante
    }

    return HttpResponse(json.dumps(valores_tot, indent=4), content_type='application/json')


def hello_word(request):
    return HttpResponse('hello word')

def sorted(request):
    numbers =  request.GET['numbers'].split(',')

    print(numbers[0])
    print(numbers[1])

    data = {
        'status': 'ok',
        'numbers': numbers,
        'message': 'Integers sorted successfully.'
    }

    return HttpResponse(json.dumps(data, indent=4), content_type='application/json')

def say_hi(request, name, age):
        if age < 12:
            message = 'sorry {}'.format(name)
            data = {
                'status': 'ok',
                'message': message,
                'value': 'Successfully.'
            }
        else:
            message = 'hello {}'.format(name)
            data = {
                'status': 'ok',
                'message': message,
                'value': 'Successfully.'
            }

        return HttpResponse(json.dumps(data, indent=4), content_type='application/json')
